import pandas as pd
from collections import defaultdict
from queue import PriorityQueue
import numpy as np
from bs4 import BeautifulSoup
from selenium import webdriver
import re

###############################################################################################################
######################################## FOR POST REQUEST    ##################################################
###############################################################################################################

# returns a storyline and movie title based on a link provided
def get_storyline(url):
    CLEANR = re.compile('<.*?>') 

    def cleanhtml(raw_html):
        cleantext = re.sub(CLEANR, '', raw_html)
        return cleantext

    browser = webdriver.Chrome('./chromedriver')
    browser.get(url)
    innerHTML = browser.execute_script("return document.body.innerHTML")

    storylines_soup = BeautifulSoup(innerHTML).findAll('div', {'class' : 'ipc-html-content ipc-html-content--base'})
    titles_soup = BeautifulSoup(innerHTML).findAll('h1', {'class': 'TitleHeader__TitleText-sc-1wu6n3d-0 dxSWFG'})
    
    storylines = []
    for storyline_html in storylines_soup:
        storylines.append(BeautifulSoup(storyline_html.text, "html.parser").text)

    titles = []
    for title_html in titles_soup:
        titles.append(BeautifulSoup(title_html.text, "html.parser").text)
        
    return storylines[0], titles[0]

# input: list of dicts of movies, must have plot and title key
def find_similarity(movies_list):
    tf_idf_input = []
    # first get the term frequency in movie_list
    for movie in movies_list:
        temp = defaultdict(int)
        words = movie['plot'].split()
        title = movie['title']
        for word in words:
            temp[word] += 1
        
        for word in temp.keys():
            if word in idf.keys():
                temp[word] *= idf[word]
            else:
                temp[word] = 0
        
        tf_idf_input.append((title, temp))
    
    # now, get the similarity
    result_dict = defaultdict(int)
    for movie, plot, plot_dict in tf_idf_movies:
        for movie1, plot_dict1 in tf_idf_input:
            for word in plot_dict1.keys():
                if word in plot_dict.keys():
                    result_dict[movie] += plot_dict[word] * plot_dict1[word]
    
    result = []
    # convert result_dict into priority queue
    for movie in result_dict.keys():
        result.append((result_dict[movie], movie))
    result.sort(reverse = True)
    return result[:10]

def get_plot(movies_list):
    result = []
    for score, movie_title in movies_list:
        temp = {}
        temp['title'] = movie_title
        temp['plot'] = data_movies[data_movies['title'] == movie_title]['plot'].values[0]
        result.append(temp)
    return result

###############################################################################################################
######################################## FOR INIT #######    ##################################################
###############################################################################################################

# input: dataframe like data_movies
# put in initialization of server
def get_tf_idf_init(movie_df):
    for index, row in movie_df.iterrows():
        words = row['plot'].split()
        title = row['title']
        tf_idf = defaultdict(int)
        # get term frequency
        for word in words:
            tf_idf[word] +=1
        
        for word in tf_idf.keys():
            tf_idf[word] *= idf[word]
            
        tf_idf_movies.append((title, row['plot'], tf_idf))