import os
import json
import flask
from flask_cors import CORS, cross_origin
from flask import request, jsonify, make_response
import pandas as pd
from collections import defaultdict
from queue import PriorityQueue
import numpy as np
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
import re
import csv


tf_idf_movies = []

app = flask.Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADER'] = 'Content-Type'
app.config['DEBUG'] = True

def get_tf_idf_init(movie_df):
    for index, row in movie_df.iterrows():
        words = row['plot'].split()
        title = row['title']
        tf_idf = defaultdict(int)
        # get term frequency
        for word in words:
            tf_idf[word] +=1
        
        for word in tf_idf.keys():
            tf_idf[word] *= float(idf[word])
            
        tf_idf_movies.append((title, row['plot'], tf_idf))
        
def get_storyline(url):
    CLEANR = re.compile('<.*?>') 

    def cleanhtml(raw_html):
        cleantext = re.sub(CLEANR, '', raw_html)
        return cleantext

    options = webdriver.ChromeOptions()
    options.add_argument('headless')
    options.add_argument('window-size=1920x1080')
    options.add_argument("disable-gpu")
    # OR options.add_argument("--disable-gpu")
    s = Service('./chromedriver')
    browser = webdriver.Chrome(service=s, options=options)
    browser.get(url)
    innerHTML = browser.execute_script("return document.body.innerHTML")

    storylines_soup = BeautifulSoup(innerHTML, features="lxml").findAll('div', {'class' : 'ipc-html-content ipc-html-content--base'})
    titles_soup = BeautifulSoup(innerHTML, features="lxml").findAll('h1', {'class': 'TitleHeader__TitleText-sc-1wu6n3d-0 dxSWFG'})
    
    storylines = []
    for storyline_html in storylines_soup:
        storylines.append(BeautifulSoup(storyline_html.text, "html.parser").text)

    titles = []
    for title_html in titles_soup:
        titles.append(BeautifulSoup(title_html.text, "html.parser").text)
        
    return storylines[0], titles[0]

# input: list of dicts of movies, must have plot and title key
def find_similarity(movies_list):
    tf_idf_input = []
    # first get the term frequency in movie_list
    for movie in movies_list:
        temp = defaultdict(int)
        words = movie['plot'].split()
        title = movie['title']
        for word in words:
            temp[word] += 1
        
        for word in temp.keys():
            if word in idf.keys():
                temp[word] *= float(idf[word])
            else:
                temp[word] = 0
        
        tf_idf_input.append((title, temp))
    
    # now, get the similarity
    result_dict = defaultdict(int)
    for movie, plot, plot_dict in tf_idf_movies:
        for movie1, plot_dict1 in tf_idf_input:
             # print(plot_dict1)
            for word in plot_dict1.keys():
                if word in plot_dict.keys():
                   # print(" ******* plot_dict *******")
                   # print(type(plot_dict[word]))
                   # print(plot_dict[word])
                   # print(" ******* plot_dict1 *******")
                   # print(type(plot_dict1[word]))
                   # print(plot_dict1[word])
                    result_dict[movie] += plot_dict[word] * plot_dict1[word]
    
    result = []
    # convert result_dict into priority queue
    for movie in result_dict.keys():
        result.append((result_dict[movie], movie))
    result.sort(reverse = True)
    return result[:10]

def get_plot(movies_list):
    result = []
    for score, movie_title in movies_list:
        temp = {}
        temp['title'] = movie_title
        temp['plot'] = movies_data[movies_data['title'] == movie_title]['plot'].values[0]
        result.append(temp)
    return result



@app.route('/api/post', methods=['POST'])
def api_add_data():
    movie_links = request.args.getlist('data')
    storylines = []
    # scrape the story lines
    for link in movie_links:
        plot, title = get_storyline(link)
        temp_dict = {}
        temp_dict['plot'] = plot
        temp_dict['title'] = title
        storylines.append(temp_dict)
    
    top_10_movies = find_similarity(storylines)
    top_10_movies_and_plot = get_plot(top_10_movies)
    
    result = json.dumps(top_10_movies_and_plot)
    
    return result, 200
    

if __name__ == '__main__':
    global movies_data
    movies_data = pd.read_csv('movies_genres.csv', error_bad_lines = False,delimiter='\t')

    # make idf dict from idf.csv
    with open('idf.csv', mode='r') as infile:
        reader = csv.reader(infile)
        with open('coors_new.csv', mode='w') as outfile:
            writer = csv.writer(outfile)
            global idf
            idf = {rows[0]:rows[1] for rows in reader}
    
    get_tf_idf_init(movies_data)
    app.run(port = 8123)
    
    
