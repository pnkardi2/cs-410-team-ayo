import React from 'react';
import Card from 'react-bootstrap/Card';

type Props = {
    title: String;
    plot: String;
}

class MovieCard extends React.Component<Props> {
    constructor(props: Props) {
        super(props);
    }
    
    render() {
        const { title, plot } = this.props;
        return (
            <Card className="movie-card">
                <Card.Body>
                    <Card.Title>{title}</Card.Title>
                    <Card.Text className="movie-plot">
                        {plot}
                    </Card.Text>
                </Card.Body>
            </Card>
        )
    }
}; 

export default MovieCard;