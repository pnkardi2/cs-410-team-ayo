import { useState, useEffect } from 'react';
import MovieCard from './components/MovieCard';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import axios from 'axios';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

type MovieObj = {
    title: String,
    plot: String
}

function App() {
    // setBookmarks(bookmarks => [...bookmarks, newBookmark]) to set the array state
    const [currentBookmarks, setBookmarks] = useState<Array<String>>([]);
    const [status, setStatus] = useState('');
    const [url, setUrl] = useState('');
    const [recommendations, setRecommendations] = useState<Array<MovieObj>>([]);
    // var recommendations = [
    //     {
    //         title: 'ShangChi',
    //         plot: 'Booboobooooboooboobooo'
    //     },
    //     {
    //         title: 'Hello',
    //         plot: 'hahahahaha'
    //     }
    // ]

    useEffect(() => {
        const queryInfo = {active: true, lastFocusedWindow: true};

        chrome.tabs && chrome.tabs.query(queryInfo, tabs => {
            var url = tabs[0].url ? tabs[0].url : '';
            setUrl(url);
        });

        setStatus('fetching...');
        // chrome.storage.sync.clear();
        /* On component mount, retrieve the recommendations for the current list of bookmarks */
        var defaultValue = "";
        chrome.storage.sync.get({links: defaultValue}, function(data) {
            // data.links will be either the stored value, or defaultValue if nothing is set
            console.log("links:",data.links);
            console.log(data.links.split(','));

            var query = 'http://localhost:8123/api/post?';
            var params: string [] = [];
            var links = data.links;
            if (links !== "") {
                links = links.split(',');

                links.forEach((bookmark: string | String) => {
                    if (bookmark !== '') {
                        params.push("data="+bookmark);
                        setBookmarks(old => [...old, bookmark]);
                    }
                });
                var params_str = params.join('&');
                query = query.concat(params_str);
                console.log("query after:", query);

                /* Get the recommendations! */
                axios.post(query).then(response => {
                    console.log("SUCCESS");
                    console.log(response);
                    response.data.forEach((movie: { title: any; plot: any; }) => {
                        let movie_obj = {
                            title: movie.title,
                            plot: movie.plot
                        }
                        setRecommendations(recs => [...recs, movie_obj]);
                    })
                    setStatus('found');
                }).catch(error => {
                    setStatus('Sorry, can\'t find any similar movies );')
                    console.log(error);
                })
            } else {
                setStatus('No Movies Yet! Start Bookmarking!');
            }
        });
    }, []);

    const recommendationsToElements = recommendations.map((movieProp, i) => (
        <MovieCard title={movieProp.title} plot={movieProp.plot} ></MovieCard>
    ));

    function onSubmit(e: { preventDefault: () => void; }) {
        e.preventDefault();
        console.log("bookmark submit!!!");
        
        // chrome.storage.sync.clear();
        setStatus('fetching...');
        var defaultValue = "";
        chrome.storage.sync.get({links: defaultValue}, function(data) {
            // data.links will be either the stored value, or defaultValue if nothing is set
            console.log("links:",data.links);
            console.log(data.links.split(','));

            var query = 'http://localhost:8123/api/post?';
            var params: string [] = [];
            var links = data.links;
            links = links.split(',');
            links.push(url);

            links.forEach((bookmark: string | String) => {
                if (bookmark !== '') {
                    params.push("data="+bookmark);
                    setBookmarks(old => [...old, bookmark]);
                }
            });
            var params_str = params.join('&');
            query = query.concat(params_str);
            console.log("query after:", query);

            /* Get the recommendations! */
            axios.post(query).then(response => {
                console.log("SUCCESS");
                console.log(response);
                response.data.forEach((movie: { title: any; plot: any; }) => {
                    let movie_obj = {
                        title: movie.title,
                        plot: movie.plot
                    }
                    setRecommendations(recs => [...recs, movie_obj]);
                })
                setStatus('found');
            }).catch(error => {
                setStatus('Sorry, can\'t find any similar movies );')
                console.log(error);
            })

            links = links.join(',');
            data.links = links;
            console.log("after:",data.links);
            chrome.storage.sync.set({links: data.links}, function() {
                // The value is now stored, so you don't have to do this again
            });
        });
        
    }

    function clearStorage(e: { preventDefault: () => void; }) {
        e.preventDefault();
        console.log("clearing storage!!");
        chrome.storage.sync.clear();
    }

    // console.log("recommendations: ", recommendations);
    console.log("current bookm",currentBookmarks);

    if (status !== 'found') {
        return (
            // check if reccomendationsToElements is empty and replace with a message if so
            <div className="App">
                <div className="container">
                    <Form
                        className="form-container"
                        method="POST"
                        onSubmit={onSubmit}>
                        <Form.Control size="lg" type="text" placeholder={url} defaultValue={url} readOnly />
                        <Button variant="primary" type="submit" style={{width: "fit-content"}}
                            disabled={status === 'fetching...' || currentBookmarks.includes(url)}>
                            Bookmark Movie Url
                        </Button>
                    </Form>
                    <Form
                        className="clear-button"
                        onSubmit={clearStorage}>
                            <Button variant="danger" type="submit" style={{width: "fit-content"}}
                            disabled={status === 'fetching...' || currentBookmarks.length === 0}>
                                Clear Bookmarks
                            </Button>
                    </Form>
                    <div className="movie-cards-container">
                        <p style={{textAlign: "center"}}>{status}</p>
                    </div>
                </div>
            </div>
        );
    }

    return (
        // check if reccomendationsToElements is empty and replace with a message if so
        <div className="App">
            <div className="container">
                <Form
                    className="form-container"
                    method="POST"
                    onSubmit={onSubmit}>
                    <Form.Control size="lg" type="text" placeholder={url} defaultValue={url} readOnly />
                    <Button variant="primary" type="submit" style={{width: "fit-content"}}
                        disabled={currentBookmarks.includes(url)}>
                        Bookmark Movie Url
                    </Button>
                </Form>
                    <Form
                        className="clear-button"
                        onSubmit={clearStorage}>
                            <Button variant="danger" type="submit" style={{width: "fit-content"}}
                            disabled={currentBookmarks.length === 0}>
                                Clear Bookmarks
                            </Button>
                    </Form>
                <div className="movie-cards-container">
                    {recommendationsToElements}
                </div>
            </div>
        </div>
    );
}

export default App;
